package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

type Request struct {
	Timestamp time.Time
	Accepted  bool
}

func parseInput() (int, int, []Request) {
	var N, R int
	fmt.Scan(&N, &R)

	requests := make([]Request, N)
	scanner := bufio.NewScanner(os.Stdin)
	for i := 0; i < N; i++ {
		if scanner.Scan() {
			timestamp, err := time.Parse(time.RFC3339, scanner.Text())
			if err != nil {
				panic(err)
			}
			requests[i] = Request{Timestamp: timestamp}
		}
	}
	return N, R, requests
}

func rateLimit(R int, requests []Request) []Request {
	windowSize := time.Hour
	for i := range requests {
		if i < R {
			requests[i].Accepted = true
		} else {
			start := requests[i].Timestamp.Add(-windowSize)
			count := 0
			for j := 0; j < i; j++ {
				if requests[j].Timestamp.After(start) && requests[j].Accepted {
					count++
				}
			}
			if count < R {
				requests[i].Accepted = true
			} else {
				requests[i].Accepted = false
			}
		}
	}
	return requests
}

func printOutput(requests []Request) {
	for _, request := range requests {
		fmt.Println(request.Accepted)
	}
}

func main() {
	_, R, requests := parseInput()
	requests = rateLimit(R, requests)
	printOutput(requests)
}
